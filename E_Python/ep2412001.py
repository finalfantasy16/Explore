#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
@Author: Yu
@Contact: EMAIL@Contact: yu0513@icloud.com
@File: ep2412001.py
@Time: 2024/12/10 10:20
@LastEditTime: 2024/12/10 10:20
@Desc: 
'''

import requests
from bs4 import BeautifulSoup
from lxml import etree
import re
import pandas as pd

index_url = "https://www.ahfgb.com"


def fetch_html(url, params):
    """
    发送请求并返回HTML内容
    """
    try:
        response = requests.get(url=url, params=params)
        print(response.url)
        response.raise_for_status()
        return response.content
    except requests.exceptions.RequestException as e:
        print(f"请求失败，错误信息：{e}")
        return None


def parse_html_css(html_content):
    """
    解析HTML内容并提取数据
    """
    # 使用BeautifulSoup解析HTML内容
    # CSS选择器
    # 解析HTML内容
    soup = BeautifulSoup(html_content, "lxml")
    # 提取数据
    # print(soup.prettify())

    result_list = []
    list1 = []
    for item in soup.select("div[class='result-game-item-detail'] h3 a"):
        title = item.get_text()
        # 去除换行符
        title = re.sub(r'[\r\n]+', '', title)
        # 去除前后空格
        title = title.strip()
        link = item.get("href")
        # 补全链接
        if link and not link.startswith("http"):
            link = index_url + link
        # print(title)
        # print(href)
        list1.append({
            "Title": title,
            "Link": link
        })

    list2 = []
    # 第一个p标签下的第二个span标签
    tags = soup.select(
        "div[class='result-game-item-info'] p:first-of-type span:nth-of-type(2)")
    for tag in tags:
        author = tag.get_text()
        # print(author)
        list2.append({
            "Author": author
        })

    if len(list1) == len(list2):
        for i in range(len(list1)):
            result_list.append({
                "Title": list1[i]["Title"],
                "Author": list2[i]["Author"],
                "Link": list1[i]["Link"]
            })

        # result_list = [item for pair in zip(list1, list2) for item in pair]
    else:
        print("两个列表的长度不一致")

    return result_list


def parse_html_xpath(html_content):
    """
    解析HTML内容并提取数据
    """
    # 使用lxml解析HTML内容
    # XPath
    tree = etree.HTML(html_content)

    result_list = []
    list1 = []
    for item in tree.xpath("//div[@class='result-game-item-detail']/h3/a/text()"):
        title = item
        # 去除换行符
        title = re.sub(r'[\r\n]+', '', title)
        # 去除前后空格
        title = title.strip()

        # print(title)
        list1.append({"Title": title})

    list2 = []
    authors = tree.xpath(
        "//div[@class='result-game-item-info']/p[1]/span[2]/text()")
    for author in authors:
        # print(author)
        list2.append({"Author": author})

    list3 = []
    links = tree.xpath("//div[@class='result-game-item-detail']/h3/a/@href")
    for link in links:
        if link and not link.startswith("http"):
            link = index_url + link
        # print(link)
        list3.append({"Link": link})

    if len(list1) == len(list2) == len(list3):
        for i in range(len(list1)):
            result_list.append({
                "Title": list1[i]["Title"],
                "Author": list2[i]["Author"],
                "Link": list3[i]["Link"]
            })
    else:
        print("三个列表的长度不一致")

    return result_list


def save_data_to_csv(data_list):
    """
    将数据保存到CSV文件
    """
    import csv
    csv_file = "data.csv"
    with open(csv_file, "w", newline="", encoding="utf-8") as file:
        writer = csv.DictWriter(file, fieldnames=["title", "author", "link"])
        writer.writeheader()
    print(f"数据已保存到 {csv_file}")


def main():
    print("Hello World!")

    url = "https://www.ahfgb.com/search.php"
    params = {
        "keyword": "末日进化乐园",
        "submit": "yes"}
    html_content = fetch_html(url, params)
    if html_content:
        # result_list = parse_html_css(html_content)
        result_list = parse_html_xpath(html_content)
        # print(result_list)
        df = pd.DataFrame(result_list)
        # 解决中英文混合的关系导致的列名不对齐的问题
        pd.set_option('display.unicode.ambiguous_as_wide', True)
        pd.set_option('display.unicode.east_asian_width', True)
        # 解决列名过长的问题
        # pd.set_option('display.max_colwidth', 200)
        print(df)
        # html_string = df.to_html()
        # print(html_string)
        # save_data_to_csv(result_list)


if __name__ == '__main__':
    main()
